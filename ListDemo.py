fruits=['banana','apple','mango','watermelon'];
fruits.reverse();
print(fruits);
fruits.append("grapes");#To append in list at last
print(fruits);
fruits.insert(2,"guava");#To insert in between the list 
print(fruits);
fruits.remove("guava");#To deleate an element from the list
print(fruits);
fruits.sort();  #To sort the list
print(fruits);
fruits.pop();  #To deleate the elementfrom last
print(fruits);

for i in fruits: #To print each element
    print(i);
    
print(type(fruits)); #To display the type of fruits
print(len(fruits));
