programmingLang=("Java","Python","C++","C","C");
print(programmingLang);
print("Value at 1st Index="+programmingLang[0]);
print("Total no of element in Tuple=",len(programmingLang));
print("Count of Java=",programmingLang.count("Java"));
for i in programmingLang:
    if i=="Java":
        variable="test"
        print("Praggramming Language is ",i);
    else:
        print("Other Language");

def addition(a,b):
    return a+b;
print("The sum is ",addition(45,5));

def addition1(a,b=45):
    return (a+b),(a-b);
print("The sum is ",addition1(5));
